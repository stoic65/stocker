var express = require("express");
let prettyjson  = require('prettyjson');
var api = express.Router();
let flock = require('flockos');
let dbController = require('../models/controller');
let config = require('../config.js');
let async = require('async');
let jwtDecode = require('jwt-decode');
let url = require('url');
let querystring = require('querystring');
let apiController = require('../apiController/controller.js')

api.get("/landing",(req,res,next)=>{
	console.log(req.get('x-flock-event-token'));
	let userId = jwtDecode(req.query.flockEventToken)["userId"];
	res.render('configuration',{"userId":userId});
});

api.post("/getStocks",(req,res,next)=>{
	console.log(req.body);
	let userId = req.body.userId;

	dbController.getStocks(userId,(err,arrayOfStocks)=>{
		if(err)res.end();
		else res.end(JSON.stringify(arrayOfStocks));

	});

});


api.post("/searchStocks",(req,res,next)=>{
	console.log("Search Stocks");
	console.log(req.body);
	let searchString = req.body.searchString;
	apiController.searchStocks(searchString,(err,resultArray)=>{
		if(err)
		{
			console.log(err);
			res.end("Error occured");
		}
		else res.end(resultArray);
	});

});

api.post("/addStocks",(req,res,next)=>{
	console.log(req.body);
	let symbol = req.body.stockSymbol;
	let userId = req.body.userId;
	console.log("token here");
	dbController.addStock(userId,symbol,(err,resultArray)=>{
		if(err){
			console.log(err)
			res.end(new Error("Error in adding stock"))
		}
		else {
			res.end(resultArray);
		}
	});
})

api.post('/deleteStock',(req,res,next)=>{
	let userId = req.body.userId;
	let symbol = req.body.stockSymbol;

	dbController.deleteStock(userId,symbol,(err)=>{
		if(err)res.end(err);
		else{
			res.end();
		}
	});



});


api.post('/updateStock',(req,res,next)=>{
	let userId = req.body.userId;
	let symbol = req.body.stockSymbol;
	let max = req.body.max;
	let min = req.body.min;

	dbController.updateStock(userId,symbol,min,max,(err)=>{
		res.end();
	});

})





module.exports  = api;