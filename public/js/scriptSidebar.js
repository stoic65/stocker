$(document).ready(function(){

	
	$.post('/configuration/getStocks',{
	 	"userId":$("#userDiv").attr('data-userid')

	 },function(data,status){
	 	console.log(data);
	 	data = JSON.parse(data);
	 	$(".main").html('');
	 	async.each(data,function(item,cb){
	 		
	 		item.price = Number(item.price).toPrecision(4);
	 		item.change= Number(item.change).toPrecision(2);
	 		item.changePercent = Number(item.changePercent).toPrecision(2);
	 		var color = item.change>=0?"green accent-4":"red accent-4";
	 		$(".main").append(
	 			`<div class="row white z-depth-3 stockCards" data-symbol=`+item.symbol+` style="border-radius: 10px;padding-top:15px;margin:5px;">
					<span><b><p style="margin:20px;display:inline">`+item.symbol+`</p></b></span><span>`+item.name+`<span>
					<div style="margin:20px">
						<span style="margin:5px"><b>Price:</b> `+item.price+` </span> <span style="color:white;padding:5px" class="`+color+`" ><b>`+item.change+`</b></span> <span style="color:white;padding:5px" class="`+color+`" ><b>`+item.changePercent+` % </b></span>
					</div>
					
				</div>`

	 			)
	 		cb();
	 	},function(err){

	 		$('.stockCards').click(function(){
	 			
	 			var currentElement = $(this);
	 			flock.openWidget('https://e32da624.ngrok.io/widgets/graph?symbolName='+currentElement.attr('data-symbol'), 'modal');



	 		});

	 	});

	 });








})