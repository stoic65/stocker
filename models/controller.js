let mongoose = require('mongoose');
let async=require('async');

mongoose.connect("mongodb://localhost/stocker",(err)=>{
	if(err)console.log(err);
	else console.log("Connected to db");

});

require('./stock.js');
require('./user.js');
let shortid = require('shortid');
let User = mongoose.model('User');
let Stock = mongoose.model('Stock');
let apiController = require('../apiController/controller.js');
let alertSender = require('../scheduler/alertSender.js');
let config = require('../config');


exports.createUser = (event,userObject,cb)=>{
	User.create({
    	"userid":event.userId,
    	"token":event.token,
    	"teamId":userObject.teamId,
    	"firstName":userObject.firstName,
   		"lastName":userObject.lastName,
    	"role":userObject.role,
    	"profileImage":userObject.profileImage
    },(err,user)=>{
    	if(err)console.log(err);
    	else cb();
   	});
}

exports.deleteUser = (userid,cb)=>{
	User.findOne({"userid":userid},(err,usertoDelete)=>{
		usertoDelete.remove((err)=>{
			if(err)console.log(err);
			else cb();
		});
	});
}


exports.getStocks = (userid,callback)=>{
	User.findOne({"userid":userid},(err,userObj)=>{
		async.map(userObj.stocks,(stockInfo,cb)=>{
			
			Stock.findOne({"symbol":stockInfo["symbol"]},(err,stockObj)=>{
				if(err){
					cb(err,null);
				}
				var newObj = {
					"min":stockInfo["min"],
					"max":stockInfo["max"],
					"symbol":stockObj["symbol"],
					"price":stockObj["price"],
					"name":stockObj["name"],
					"change":stockObj["change"],
					"changePercent":stockObj["changePercent"]
				}

				cb(null,newObj);
			});

		},(err,resultArray)=>{
			console.log(resultArray);
			if(err)
			{
				console.log(err);
				callback(err,null);
			}
			
			callback(null,resultArray);
		});
	});
}

exports.addStock = (userid,symbol,callback)=>{

	apiController.getQuotes(symbol,(stockInfo)=>{
		Stock.findOne({"symbol":symbol},(err,stockObj)=>{
			if(err||stockObj==null)
			{
				Stock.create({
					"symbol":symbol,
					"price":stockInfo["Price"],
					"name":stockInfo["Name"],
					"change":stockInfo["Change"],
					"changePercent":stockInfo["ChangePercent"],
					"userAssociated":[userid]
				},(err,createdObj)=>{
					if(err)console.log(err);
				});
			}
			else
			{
				stockObj.userAssociated.push(userid);
				stockObj.save((err,stockSaved)=>{
					if(err)console.log(err);
				});
			}
		});
		User.findOne({"userid":userid},(err,userObj)=>{
			if(err)console.log(err);
			async.each(userObj.stocks,(item,cb1)=>{
				if(item['symbol'] == symbol)cb1(new Error("Already exists"))
				else cb1(null);
			},(err)=>{
				if(err)console.log(err);
				else{
					userObj.stocks.push({"symbol":symbol,"min":null,"max":null})
					userObj.save((err,savedUser)=>{
						if(err)console.log(err);
					});
				}
				callback();
			});



			

		});

	});


}

exports.deleteStock = (userId,symbol,callback)=>{
	async.parallel([(cb1)=>{
		User.findOne({userid:userId},(err,userObj)=>{

			async.filter(userObj.stocks,(item,cb)=>{
				if(item["symbol"] == symbol)cb(null,false);
				else cb(null,true);

			},function(err,resultArray){
				userObj.stocks = resultArray;
				userObj.save((err,obj)=>{
					cb1();
				});
			});
			
		});

	},(cb1)=>{

		Stock.findOne({symbol:symbol},(err,stockObj)=>{
			async.filter(stockObj.userAssociated,(item,cb)=>{
				if(item==userId)cb(null,false);
				else cb(null,true);
			},(err,resultArray)=>{
				if(resultArray.length==0){
					Stock.remove({symbol:symbol},()=>{
						console.log("removed");
						cb1();
					});
				}
				else {
					stockObj.userAssociated = resultArray;
					stockObj.save((err,obj)=>{
						cb1();
					})
				}
			});
		});

	}],(err,result)=>{
		if(err)console.log(err);
		else callback();

	})

}

exports.updateStock = (userid,symbol,min,max,callback)=>{

	User.findOne({userid:userid},(err,userObj)=>{
		async.map(userObj.stocks,(item,cb)=>{
			if(item["symbol"]==symbol){
				item["max"] = max;
				item["min"] = min;
				cb(null,item);
			}
			else cb(null,item);
		},(err,resultArray)=>{
			userObj.stocks = resultArray;
			userObj.save((err,savedObj)=>{
				console.log(savedObj);
				callback();
			});
		});

	});


}


exports.updateAllStocks = (callback)=>{
	Stock.find({},(err,arrayOfStocks)=>{
		delayCounter = 0;
		async.eachSeries(arrayOfStocks,(item,cb)=>{
			apiController.getQuotes(item["symbol"],(stockQuote)=>{
				
				item["price"] = Number(stockQuote["Price"])+config.modifier;
				item["change"] = stockQuote["Change"];
				item["changePercent"] = stockQuote["ChangePercent"];
				


				item.save((err,savedStockObj)=>{
					console.log(savedStockObj);
					async.each(savedStockObj.userAssociated,(item2,cb2)=>{
						User.findOne({userid:item2},(err,userObj)=>{
							if(err)console.log(err);
							async.map(userObj.stocks,(item3,cb3)=>{
								
								if(item3["symbol"]==savedStockObj["symbol"])
								{
									if(item3["min"] && savedStockObj["price"]<item3["min"])
									{	
										alertSender.sendAlert(userObj,savedStockObj,item3,false,()=>{
		
											item3["min"] = null;
											cb3(null,item3);
										
										});
									}
									else if(item3["max"] && savedStockObj["price"]>item3["max"])
									{	
										alertSender.sendAlert(userObj,savedStockObj,item3,true,()=>{
											item3["max"] = null;
											cb3(null,item3);
										});		
									}
									else cb3(null,item3)
								}
								else cb3(null,item3);

							},(err,resultArray)=>{
								userObj.stocks = resultArray;
								userObj.save((err,userSavedObj)=>{
									cb2(null);
								});
							});


						});
					},(err)=>{
						setTimeout(function(){delayCounter+=3000;cb();},1000+delayCounter);
						

					});
				});
			});

		},(err)=>{
			callback();

		});






		
	});
	


}











