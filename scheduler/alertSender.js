var express = require("express");
let prettyjson  = require('prettyjson');
var api = express.Router();
let flock = require('flockos');
let dbController = require('../models/controller');
let config = require('../config.js');
let async = require('async');

let url = require('url');
let querystring = require('querystring');


exports.sendAlert  = (userObj,stockObj,maxMinObj,isMax,callback)=>{

	var text = '';
	if(isMax)
	{
		text = `Hi, the stock `+stockObj.symbol+` has crossed its limit price of `+maxMinObj.max+` and is currently at `+stockObj.price+`. Limit price has been reset. Please visit the config page to set it again`;
	}
	else {
		text = `Hi, the stock `+stockObj.symbol+` has crossed its stop loss price of `+maxMinObj.min+` and is currently at `+stockObj.price+`. Stop loss price has been reset. Please visit the config page to set it again`;
	}


	flock.callMethod('chat.sendMessage',config.botToken,{
		to:userObj["userid"],
		text:text
	},(response)=>{
		console.log("response")

	})
	callback();

}
