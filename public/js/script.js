

$(document).ready(function(){
	$(".modal").modal();
	console.log($(".modal"));

	 $("#submitLocation").click(function(){
	 	$("#symbolLoader").show();
      	
        $(".modal-content").html('');
        $.post("/configuration/searchStocks",
        {
          searchString:$("#location").val()
        },
        function(data,status){
        	data = JSON.parse(data);
        	$("#symbolLoader").hide();
        	$(".modal-content").append('<h4>Choose the stock you want to add</h4>');
        	async.each(data,function(item,callback){
        		$(".modal-content").append('<div class="row white z-depth-3 symbolList" data-symbol='+item.Exchange+':'+item.Symbol+' style="border-radius: 10px;max-width:100%;margin-top:20px;margin-left: 0%;text-align:center;"><div class="input-field col s12 m4 l4 " style="text-align:center"><p>'+item.Symbol+'</p></div><div class="input-field col s12 m6 l3" style="text-align:center"><p >'+item.Name+'</p> </div> <div class="input-field col s12 m4 l4" style="text-align:center"> <p >'+item.Exchange+'</p> </div> <div class="input-field col s12 m4 l4" style="text-align:center">  </div> ')
        		callback(null);
        	},function(err){
        		$(".symbolList").click(function(){

        			var symbol = $(this).attr('data-symbol')

        			$.post('/configuration/addStocks',{
        				"stockSymbol":symbol,
        				"userId":$("#userDiv").attr('data-userid')
        			},function(data,status){
        				$(".modal").modal('close');
        				location.reload();
        			});
				});
        		if(err)console.log(err);
        	});
        	console.log(data);

        })
    });


	 $.post('/configuration/getStocks',{
	 	"userId":$("#userDiv").attr('data-userid')

	 },function(data,status){

	 	console.log(data);
	 	data = JSON.parse(data);
	 	async.each(data,function(item,cb){
	 		item.price = Number(item.price).toPrecision(4);
	 		item.change= Number(item.change).toPrecision(2);
	 		item.changePercent = Number(item.changePercent).toPrecision(2);
	 		var color = item.change>=0?"green accent-4":"red accent-4";
	 		$("#stockDisplay").append(`<div class="row white z-depth-3" style="border-radius: 10px;max-width: 95%;margin-top:20px;margin-left: 2.5%" data-symbol=`+item.symbol+` data-price=`+item.price+`>
			      <div class="col s12 m6 l3 "  style="padding-top:5px;margin-top:20px;text-align: center">
			             <div class="col s12" id="symbol">`+item.symbol+`</div>
			             <div class="col s12" id="name">`+item.name+`</div>
			      </div>
			       <div class="col s12 m6 l3 "  style="padding-top:5px;margin-top:20px">
			             <div class="col s6"  style="text-align: center;color:white">
			             <div class="col s12 `+ color+` " id="change">`+item.change+`</div>
			             <div class="col s12 `+ color+` " id="percentage">`+item.changePercent+`%</div>
			             </div>
			             <div class="col s6" style="text-align: center">
			             <div class="col s12">PRICE</div>
			             <div class="col s12" id="price">`+item.price+`</div>
			             </div>
			      </div>
			      <div class="col s12 m6 l3" data-test1="something">
			            <div class="input-field col s6">
			           	 	<input  type="number" step="0.01" class="validate minVal" placeholder="Min" value=`+item.min+`>
			            </div>
			            <div class="input-field col s6">
			            	<input  type="number" step="0.01" class="validate maxVal" placeHolder="Max" value=`+item.max+`>
			            </div>
			      </div>
			      <div class="col s12 m6 l3 style="margin:40px;">
			        <a class="btn-floating btn-large waves-effect purple darken-4"  style="margin-top:15px; margin-left:30%;margin-bottom:10%">
			          <i class="material-icons updateStock">done</i>
			        </a>
			        
			        <a class="btn-floating btn-large waves-effect purple darken-4 "   style="margin-top:15px;margin-bottom:10%">
			          <i class="material-icons deleteStock">clear</i>
			        </a>
			        </div>
			         
			    </div> 
			`);
			cb();
	 	},function(err){
	 		console.log("done loading");
	 		$(".deleteStock").click(function(e){

	 			var deleteSymbol = $(e.target).parent().parent().parent().attr("data-symbol");
	 			$.post('/configuration/deleteStock',{
	 				"userId":$("#userDiv").attr('data-userid'),
	 				"stockSymbol":deleteSymbol
	 			},function(data,status){
	 				location.reload();
	 				console.log("deleted");
	 			})
	 

	 		});

	 		$(".updateStock").click(function(e){
	 			var updateSymbol = $(e.target).parent().parent().parent().attr("data-symbol");
	 			console.log($($($($(e.target).parent().parent().parent().children()[2]).children())[0]));

	 			var minString = $($($($(e.target).parent().parent().parent().children()[2]).children()[0]).children()[0]).val();
	 			var maxString = $($($($(e.target).parent().parent().parent().children()[2]).children()[1]).children()[0]).val();
	 			
	 			let min;
	 			let max;

	 			if(minString)
	 				min = Number(minString);
	 			else 
	 				min = null;

	 			if(maxString)
	 				max = Number(maxString);
	 			else 
	 				max = null;
	 			
	 			var price = Number($(e.target).parent().parent().parent().attr("data-price"));

	 			console.log("max,min ",min,max,minString,maxString)

	 			if(  ((min==null)||(min<price)) && ((max==null)||(max>price)) )
	 			{
	 				$.post('/configuration/updateStock',{
	 					"userId":$("#userDiv").attr('data-userid'),
	 					"stockSymbol":updateSymbol,
	 					"max":max,
	 					"min":min
	 				},function(data,status){
	 					console.log("Updated");
	 					location.reload();
	 				});
	 			}
	 			else
	 			{
	 				location.reload();
	 				return;
	 			}

	 		});
	 		






	 	});
	 	
	 	

	 })

});