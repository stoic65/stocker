let markit = require("./markit.js");


exports.searchStocks = (searchString,callback)=>{
	markit.searchSymbol(searchString,(symbolArray)=>{
		if(symbolArray==null)
		{
			callback(new Error("No symbol found"),null);
		}
		callback(null,symbolArray);
	})
}

exports.getQuotes = (symbol,cb)=>{
	markit.getQuotes(symbol,(result)=>{
		cb(result);
	});
}