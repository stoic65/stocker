var express = require("express");
let prettyjson  = require('prettyjson');
var api = express.Router();
let flock = require('flockos');
let dbController = require('../models/controller');
let config = require('../config.js');
let async = require('async');
let jwtDecode = require('jwt-decode');
let url = require('url');
let querystring = require('querystring');
let apiController = require('../apiController/controller.js')



api.get('/sidebar',(req,res,next)=>{
	//console.log(req);
	let userId = jwtDecode(req.query.flockEventToken)["userId"];
	res.render('sidebar',{"userId":userId});
});


api.post('/share',(req,res,next)=>{

	console.log(req);
	let query = querystring.parse(url.parse(req.headers.referer).query);
	let FlockEvent = JSON.parse(query.flockEvent);	
	console.log(FlockEvent);
	res.end();

});

api.get('/graph',(req,res,next)=>{
	console.log(req.query);
	res.render('graph',{symbol:req.query.symbolName});
});




module.exports  = api;

