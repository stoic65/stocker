let mongoose = require('mongoose');

let userSchema  = new mongoose.Schema({
	"userid":{type:String, unique:true},
	"token":String,
	"teamId":String,
	"email":String,
	"firstName":String,
	"lastName":String,
	"role":String,
	"profileImage":String,
	"stocks":[{"symbol":String,"min":Number,"max":Number}]
});

mongoose.model("User",userSchema);