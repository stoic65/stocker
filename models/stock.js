let mongoose = require('mongoose');

let stockSchema  = new mongoose.Schema({
	"symbol":{type:String, unique:true},
	"price":Number,
	"name":String,
	"change":Number,
	"changePercent":Number,
	"userAssociated":[String]
});

mongoose.model("Stock",stockSchema);