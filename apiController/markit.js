'use strict'
var request = require('request');

exports.searchSymbol = (SearchString,cb) =>{
	let opts={
		url:"http://dev.markitondemand.com/Api/v2/Lookup/json",
		qs:{
			input:SearchString
		}
	};
	request.get(opts,function(err,res,body){
		cb(body);
	});
};

exports.getQuotes = (Symbol,cb) =>{
	Symbol = Symbol.split(':')[1];
	let opts={
		url:"http://dev.markitondemand.com/Api/v2/Quote/json",
		qs:{
			symbol:Symbol
		}
	};
	request.get(opts,function(err,res,body){
		body=JSON.parse(body);
		let reqObj={
			"Name":body.Name,
			"Symbol":body.Symbol,
			"Price":body.LastPrice,
			"Change":body.Change,
			"ChangePercent":body.ChangePercent
		};
		cb(reqObj);
	});
}; 